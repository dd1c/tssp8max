

public class StopWatch {
    private long startTime;
    private boolean isStarted = false;
    private long pauseStartTime;
    private long pauseTime;

    public void start() {
        if (!isStarted) {
            startTime = System.currentTimeMillis();
            isStarted = true;
        }
    }


    public String stop() {
        if (!isStarted) {
            return 0 + ":" + 0 + ":" + 0;
        }
        long stopTime = System.currentTimeMillis();
        long time = (stopTime - startTime) - pauseTime;
        int minutes = (int) ((time / 60000) % 24);
        int seconds = (int) ((time / 1000) % 60);
        int milsec = (int) ((time / 10) % 100);
        isStarted = false;
        return minutes + ":" + seconds + ":" + milsec;

    }

    public String startPause() {
        pauseStartTime = System.currentTimeMillis();
        long time = pauseStartTime - startTime;
        int minutes = (int) ((time / 60000) % 24);
        int seconds = (int) ((time / 1000) % 60);
        int milsec = (int) ((time / 10) % 100);

        return minutes + ":" + seconds + ":" + milsec;
    }

    public void stopPause() {
        pauseTime = System.currentTimeMillis() - pauseStartTime;
    }

}
