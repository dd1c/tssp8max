import java.util.Scanner;

import static java.lang.System.exit;

public class Main {
    public static void printMenu(String[] options) {
        for (String option : options) {
            System.out.println(option);
        }
        System.out.print("Choose your option : ");
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        String[] options = {"1- Start",
                "2- Stop",
                "3- Pause",
                "4- Release",
                "5-Exit"
        };
        Scanner scanner = new Scanner(System.in);
        int option = 1;
        while (option != 5) {
            printMenu(options);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 1 -> stopWatch.start();
                    case 2 -> System.out.println("Time: " + stopWatch.stop());
                    case 3 -> System.out.println("Current Time" + stopWatch.startPause());
                    case 4 -> stopWatch.stopPause();
                    case 5 -> exit(0);
                }
            } catch (Exception ex) {
                System.out.println("Please enter an integer value between 1 and " + options.length);
                scanner.next();
            }
        }
    }

}
