import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StopWatchTest {


    @Test
    void test1() throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Thread.sleep(8000);
        assertTrue(85 > Integer.parseInt(stopWatch.stop().replaceAll(":","")));
    }

    @Test
    void test2() {
        StopWatch stopWatch = new StopWatch();
        assertEquals("0:0:0", stopWatch.stop());

    }


    @Test
    void test3() throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Thread.sleep(3000);
        stopWatch.start();
        assertTrue(35 > Integer.parseInt(stopWatch.stop().replaceAll(":","")));
    }

    @Test
    void test4() throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Thread.sleep(4000);
        stopWatch.startPause();
        Thread.sleep(3000);
        stopWatch.stopPause();
        Thread.sleep(4000);
        assertTrue(85 > Integer.parseInt(stopWatch.stop().replaceAll(":","")));
    }
    @Test
    void test5() throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Thread.sleep(72000);
        assertEquals("1:12:0", stopWatch.stop());
    }


}